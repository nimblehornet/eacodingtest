# eacodingtest

For this test we are asking you to build an application that consumes our json service that is available in our Swagger documentation.

You&#39;ll need to build a service that consumes this API, and produces a list of the models of cars and the show they attended, grouped by make alphabetically.